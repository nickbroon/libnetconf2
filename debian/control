Source: libnetconf2
Section: libs
Maintainer: Ondřej Surý <ondrej@debian.org>
Priority: optional
Standards-Version: 4.5.1.0
Build-Depends: cmake,
               debhelper-compat (= 12),
               doxygen,
               graphviz,
               libssh-dev,
               libssl-dev,
               libyang2-dev,
               pkg-config
Homepage: https://github.com/CESNET/libnetconf2

Package: libnetconf2-doc
Depends: ${misc:Depends}
Section: doc
Architecture: all
Multi-Arch: foreign
Description: NETCONF protocol library [docs]
 NETCONF library in C intended for building NETCONF clients and servers. NETCONF
 is the NETwork CONFiguration protocol introduced by IETF.
 .
 libnetconf2 is a NETCONF library in C handling NETCONF authentication and all
 NETCONF RPC communication both server and client-side. Note that NETCONF
 datastore implementation is not a part of this library. The library supports
 both NETCONF 1.0 (RFC 4741) as well as NETCONF 1.1 (RFC 6241). The main
 features include:
 .
  * NETCONF over SSH (RFC 4742, RFC 6242), using libssh.
  * NETCONF over TLS (RFC 7589), using OpenSSL.
  * DNSSEC SSH Key Fingerprints (RFC 4255)
  * NETCONF over pre-established transport sessions (using this mechanism the
    communication can be tunneled through sshd(8), for instance).
  * NETCONF Call Home (RFC 8071).
  * NETCONF Event Notifications (RFC 5277),
 .
 This package contains the documentation.

Package: libnetconf2-2
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Section: libs
Architecture: any
Multi-Arch: same
Description: NETCONF protocol library [C library]
 NETCONF library in C intended for building NETCONF clients and servers. NETCONF
 is the NETwork CONFiguration protocol introduced by IETF.
 .
 libnetconf2 is a NETCONF library in C handling NETCONF authentication and all
 NETCONF RPC communication both server and client-side. Note that NETCONF
 datastore implementation is not a part of this library. The library supports
 both NETCONF 1.0 (RFC 4741) as well as NETCONF 1.1 (RFC 6241). The main
 features include:
 .
  * NETCONF over SSH (RFC 4742, RFC 6242), using libssh.
  * NETCONF over TLS (RFC 7589), using OpenSSL.
  * DNSSEC SSH Key Fingerprints (RFC 4255)
  * NETCONF over pre-established transport sessions (using this mechanism the
    communication can be tunneled through sshd(8), for instance).
  * NETCONF Call Home (RFC 8071).
  * NETCONF Event Notifications (RFC 5277),
 .
 This package contains the shared C library.

Package: libnetconf2-dev
Depends: libnetconf2-2 (= ${binary:Version}),
         libyang2-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Section: libdevel
Architecture: any
Multi-Arch: same
Description: NETCONF protocol library [C development]
 NETCONF library in C intended for building NETCONF clients and servers. NETCONF
 is the NETwork CONFiguration protocol introduced by IETF.
 .
 libnetconf2 is a NETCONF library in C handling NETCONF authentication and all
 NETCONF RPC communication both server and client-side. Note that NETCONF
 datastore implementation is not a part of this library. The library supports
 both NETCONF 1.0 (RFC 4741) as well as NETCONF 1.1 (RFC 6241). The main
 features include:
 .
  * NETCONF over SSH (RFC 4742, RFC 6242), using libssh.
  * NETCONF over TLS (RFC 7589), using OpenSSL.
  * DNSSEC SSH Key Fingerprints (RFC 4255)
  * NETCONF over pre-established transport sessions (using this mechanism the
    communication can be tunneled through sshd(8), for instance).
  * NETCONF Call Home (RFC 8071).
  * NETCONF Event Notifications (RFC 5277),
 .
 This package contains the static C library and headers.
